﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    private GameManager gm = null;

    void OnEnable()
    {
        gm = (GameManager)target;
    }
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        EditorGUILayout.PrefixLabel("Найстройки лучника");
        EditorGUILayout.BeginVertical("box");
        gm.ArcherMaxHealth = EditorGUILayout.FloatField("Очки жизни лучника", gm.ArcherMaxHealth);
        gm.HealthRecoveryMultipler = EditorGUILayout.FloatField("Множитель восстановления здоровья", gm.HealthRecoveryMultipler);
        gm.ShootInverval = EditorGUILayout.FloatField("Интервал между выстрелами", gm.ShootInverval);
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        EditorGUILayout.PrefixLabel("Найстройки игрового уровня");
        EditorGUILayout.BeginVertical("box");
        gm.ArcherSpeed = EditorGUILayout.FloatField("Скорость лучника", gm.ArcherSpeed);
        gm.EnemySpawnInterval = EditorGUILayout.FloatField("Интервал спавна врагов", gm.EnemySpawnInterval);
        gm.ObstacleSpawnInterval = EditorGUILayout.FloatField("Интервал спавна препятсвий", gm.ObstacleSpawnInterval);
        EditorGUILayout.EndVertical();

        if (GUI.changed) SetObjectDirty(gm.gameObject);
    }
    static void SetObjectDirty(GameObject obj)
    {
        EditorUtility.SetDirty(obj);
        if (!EditorApplication.isPlaying)
            EditorSceneManager.MarkSceneDirty(obj.scene);
    }
}
