﻿using System.Collections;
using UnityEngine;

//Этот скрипт отвечает за прикольную анимацию удара противника. Я не реализовал это в Enemy, чтобы сделать гибкость и в случае чего можно вместо кружков
//поставить что-то другое и с анимацией
[RequireComponent(typeof(Enemy))]
public class EnemyAnimator : MonoBehaviour
{
    public Transform enemyArrow = null;
    Enemy enemy = null;

    Vector3 startArrowPos;
    void Start()
    {
        enemy = GetComponent<Enemy>();
        startArrowPos = enemyArrow.localPosition;
    }
    //метод, вызываемый в onAttack в скрипте Enemy
    public void PushArrow()
    {
        StartCoroutine(arrowAnim());
    }
    IEnumerator arrowAnim()
    {
        //Этот вектор нужен, чтобы стрелка не улетала вверх или вниз
        var targetPoint = new Vector3(enemy.Target.position.x, enemyArrow.localPosition.y, enemy.Target.position.z);
        //вектор, направленный на target
        var obser = (enemyArrow.localPosition - targetPoint).normalized * enemy.AttackDistance;

        var enemyPos = enemyArrow.localPosition + obser;

        for (float t = 0f; t <= 1f; t += Time.deltaTime * 10f)
        {
            enemyArrow.localPosition = Vector3.Lerp(startArrowPos, enemyPos, t);
            yield return new WaitForEndOfFrame();
        }
        for (float t = 0f; t <= 1f; t += Time.deltaTime)
        {
            enemyArrow.localPosition = Vector3.Lerp(enemyPos, startArrowPos, t);
            yield return new WaitForEndOfFrame();
        }
    }
}
