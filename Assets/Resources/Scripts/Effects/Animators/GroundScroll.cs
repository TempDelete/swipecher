﻿using UnityEngine;

[RequireComponent(typeof(Material))]
public class GroundScroll : MonoBehaviour
{
    private Renderer _renderer;
    private Vector2 offset;

    void Start()
    {
        _renderer = GetComponent<Renderer>();
        offset = Vector2.zero;
    }

    void FixedUpdate()
    {
        float x = Mathf.Repeat(Time.time * GameManager.instance.ArcherSpeed * GameManager.groundSpeedMultipler, 1);
        Vector2 offset = new Vector2(0, x);
        _renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
