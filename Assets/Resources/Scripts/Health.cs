﻿using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float HealthPoint = 100f;
    public float MaxHealthPoint = 100f;

    public UnityEvent onDamage = null;
    public UnityEvent onDie = null;

    public void Damage(float dmg)
    {
        HealthPoint -= dmg;
        if (HealthPoint <= 0f)
            Die();
        onDamage.Invoke();
    }
    public void Die()
    {
        onDie.Invoke();
    }
}
