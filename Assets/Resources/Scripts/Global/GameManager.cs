﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float HealthRecoveryMultipler = 1f;   //Влияет на скорость восстановления здоровья
    public float ArcherMaxHealth = 100f; //Очки жизни лучника
    public float ArcherSpeed = 0.4f; //Скорость движения лучника
    public float ShootInverval = 0.4f; //Интервал между выстрелами лучника (в секундах)
    public float ObstacleSpawnInterval = 3.5f; //Интервал между спавном препятствий (в секундах)
    public float EnemySpawnInterval = 3f; //Интервал между спавном противников (в секундах)

    public const float groundSpeedMultipler = 0.4f;  //Множитель для синхронизации скорости движения земли и противников
    public const float enemyObstacleSpeedMultipler = 4.1f;   //Этот коэфициент отвечает за синхронизацию скоростей движения земли и противников с препятствиями
    
    public static GameManager instance = null;
    
    public uint enemyKilled = 0;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Multiple gameManagers detected");
            Destroy(this);
        }
        else instance = this;
        enabled = false;
    }
    //Костыль был реализован в из-за того, что настроенные значения переменных CustomInspector сбрасываются при включении игры
    //Тут такая псевдоинкапсуляция
    private void LateUpdate()
    {
        ArcherMaxHealth = Mathf.Abs(ArcherMaxHealth);
        ArcherSpeed = Mathf.Abs(ArcherSpeed);
        ShootInverval = Mathf.Abs(ShootInverval);
        ObstacleSpawnInterval = Mathf.Abs(ObstacleSpawnInterval);
        EnemySpawnInterval = Mathf.Abs(EnemySpawnInterval);
    }
    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
