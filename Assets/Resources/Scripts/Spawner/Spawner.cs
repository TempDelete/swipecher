﻿using System.Linq;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public GameObject enemy = null;
    public GameObject obstacle = null;

    [SerializeField] Point[] enemySpawnPoints = null;
    [SerializeField] Point[] obstacleSpawnPoints = null;

    Transform nextSpawnObstaclePoint = null;
    Transform nextSpawnEnemyPoint = null;

    private void Start()
    {
        StartCoroutine(EnemySpawner());
        StartCoroutine(ObstacleSpawner());
    }
    IEnumerator EnemySpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameManager.instance.EnemySpawnInterval);

            var availablePoints = enemySpawnPoints.Where(x => x.Available).ToArray();
            if (availablePoints.Length == 0)
            {
                Debug.LogError("Too many enemies and so few spawn points");
                continue;
            }
            availablePoints[Random.Range(0, availablePoints.Length)].Spawn(enemy);
        }
    }
    IEnumerator ObstacleSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(GameManager.instance.ObstacleSpawnInterval);

            var availablePoints = obstacleSpawnPoints.Where(x => x.Available).ToArray();
            availablePoints[Random.Range(0, availablePoints.Length)].Spawn(obstacle);
        }
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
