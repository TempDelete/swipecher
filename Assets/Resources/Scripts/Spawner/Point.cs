﻿using UnityEngine;

public class Point : MonoBehaviour
{
    public bool Available => _rechargeTimer <= 0f;
    [SerializeField] float RechargeTime = 3f;   //Чтобы не было спавна обьектов друг на друга, решил сделать "перезарядку" точек спавна
    float _rechargeTimer = 0f;
    
    public void Spawn(GameObject obj)
    {
        Instantiate(obj, transform.position, transform.rotation);
        _rechargeTimer = RechargeTime;
    }

    void Update()
    {
        if(_rechargeTimer > 0f)
        _rechargeTimer -= Time.deltaTime;
    }
}
