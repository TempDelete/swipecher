﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Health))]
public class Archer : MonoBehaviour
{
    [SerializeField] Slider healthBar = null;
    [SerializeField] Text healthBarValue = null;
    [SerializeField] Text killedEnemies = null;

    List<float> shootAngles = new List<float>();
    [SerializeField] Transform arrow = null;
    [SerializeField] Transform shootVectorsRoot = null;
    [SerializeField] Transform shootVector = null;

    public static Archer instance { get; private set; } = null;

    Health health = null;
    bool canAttack = true;
    int shootInd = 0;

    void Start()
    {
        if (instance != null)
        {
            Debug.LogError("Multiple archers detected");
            Destroy(this);
        }
        else
        {
            instance = this;
            health = GetComponent<Health>();
            SwipeManager.instance.onSwipe += OnSwipe;
            SwipeManager.instance.onTap += OnTap;
            OnMaxHealthChanged();
            health.HealthPoint = health.MaxHealthPoint;
        }
    }
    void Update()
    {
        if (shootAngles.Count > 0 && canAttack)
            Shoot(shootInd);
        killedEnemies.text = GameManager.instance.enemyKilled.ToString();
        health.HealthPoint = Mathf.Clamp(health.HealthPoint + Time.deltaTime * GameManager.instance.HealthRecoveryMultipler, 0f, health.MaxHealthPoint);
        healthBar.value = health.HealthPoint / health.MaxHealthPoint;
        healthBarValue.text = string.Format("{0:0.0} / {1:0.0}", health.HealthPoint, health.MaxHealthPoint);
        if (health.MaxHealthPoint != GameManager.instance.ArcherMaxHealth)
            OnMaxHealthChanged();
    }
    private void OnMaxHealthChanged()
    {
        //var ratio = health.HealthPoint / health.MaxHealthPoint;
        //health.HealthPoint = health.MaxHealthPoint * ratio;
        health.MaxHealthPoint = GameManager.instance.ArcherMaxHealth;
    }
    private void Shoot(int i)
    {
        StartCoroutine(AttackTimeOut());
        var arr = Instantiate(arrow, shootVectorsRoot.position, Quaternion.Euler(0f, shootAngles[i], 0f));
        shootInd = (int)Mathf.Repeat(shootInd + 1, shootAngles.Count);
    }

    public void onDamage()
    {
    }

    public void onDie()
    {
        StartCoroutine(remover());
        Destroy(gameObject);
    }
    private void OnSwipe(Vector2 swipe)
    {
        if (this == null)   //В момент смерти лучника игрок может свайпнуть по экрану, это вызовет этот метод и исключение, поэтому тут и стоит эта странная проверка
            return;
        var angle = Mathf.Atan2(swipe.x, swipe.y) * Mathf.Rad2Deg;
        shootAngles.Add(angle);
        Instantiate(shootVector, transform.position, Quaternion.Euler(90f, 0f, -angle), shootVectorsRoot);
        shootInd = 0;
    }
    private void OnTap()
    {
        shootInd = 0;
        //Костыль, это необходимо, чтобы не вылезало ошибки в момент перезагрузки уровня
        /*
            MissingReferenceException: The object of type 'Archer' has been destroyed but you are still trying to access it.
            Your script should either check if it is null or you should not destroy the object.
            UnityEngine.MonoBehaviour.StartCoroutine (System.Collections.IEnumerator routine) (at <ee47be73f7ef409ca5e5ce4b121745b7>:0)
            Archer.OnTap () (at Assets/Resources/Scripts/DynamicObjects/Archer.cs:71)
            SwipeManager.SwipeDetection () (at Assets/Resources/Scripts/Controls/SwipeManager.cs:53)
            SwipeManager.Update () (at Assets/Resources/Scripts/Controls/SwipeManager.cs:31)
        */
        if (this != null)
            StartCoroutine(remover());
    }
    //По каким-то причинам Unity уходит в вечные раздумья, если в методе OnTap запустить цикл, который есть в remover
    //Еще стоит поискать решение на эту тему
    IEnumerator remover()
    {
        while (shootVectorsRoot.childCount > 0)
        {
            Destroy(shootVectorsRoot.GetChild(0).gameObject);
            yield return null;
        }
        shootAngles.Clear();
    }

    IEnumerator AttackTimeOut()
    {
        canAttack = false;
        yield return new WaitForSeconds(GameManager.instance.ShootInverval);
        canAttack = true;
    }
}
