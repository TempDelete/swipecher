﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour
{
    public float AttackDistance => attackDistance;
    public Transform Target => target;

    [SerializeField] float damage = 2f;
    [SerializeField] float enemySpeed = 2f;
    [SerializeField] float attackDistance = 2f;
    [SerializeField] float attackInterval = 1.5f;

    NavMeshAgent agent = null;
    Transform target = null;

    bool canAttack = true;

    public UnityEvent onAttack = null;

    private void Start()
    {
        if (Archer.instance == null)
        {
            Debug.LogError("Could not find target");
            enabled = false;
        }
        else
        {
            agent = GetComponent<NavMeshAgent>();
            agent.speed = GameManager.instance.ArcherSpeed * GameManager.enemyObstacleSpeedMultipler + enemySpeed; //скорость движения лучника + скорость противника
            target = Archer.instance.transform;
        }
    }
    void Update()
    {
        if(target == null)  //В случае убийства игрока, оставшиеся противники тоже исчезают
        {
            Destroy(gameObject);
            return;
        }
        MoveToTarget();
        if (Vector3.Distance(transform.position, target.position) <= attackDistance)
            TryAttack();
    }
    void MoveToTarget()
    {
        var obser = (transform.position - target.position).normalized * attackDistance * 0.8f;  //прибавление этого вектора позволяет не подходит к игроку вплотную
        agent.SetDestination(target.position + obser);
        //делает противника всегда направленным на цель (нужно, потому что когда противник становится к игроку в плотную, то может повернуться спиной)
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-obser), Time.deltaTime * 15f);
    }
    void TryAttack()
    {
        if (canAttack)
        {
            onAttack.Invoke();
            Archer.instance.SendMessage("Damage", damage);
            StartCoroutine(AttackTimeOut());
        }
    }

    public void onDamage()
    {
        //Можно воспроизводить хоть анимацию удара, хоть отнятие healthbar'а и т.д.
    }

    public void onDie()
    {
        GameManager.instance.enemyKilled++;
        Destroy(gameObject);
    }
    IEnumerator AttackTimeOut()
    {
        canAttack = false;
        yield return new WaitForSeconds(attackInterval);
        canAttack = true;
    }
}
