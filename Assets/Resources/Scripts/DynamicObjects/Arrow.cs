﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Arrow : MonoBehaviour
{
    [SerializeField] float damage = 5f;
    [SerializeField] float arrowForce = 5f;
    [SerializeField] float liveTime = 3f;

    Rigidbody body = null;
    void Start()
    {
        body = GetComponent<Rigidbody>();
        body.AddForce(transform.forward * arrowForce);
        Destroy(gameObject, liveTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Destroy(gameObject);
            return;
        }
        var health = other.GetComponent<Health>();
        if (health != null)
        {
            health.Damage(damage);
        }
    }
}
