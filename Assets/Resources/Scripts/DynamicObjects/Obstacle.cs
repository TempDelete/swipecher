﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Obstacle : MonoBehaviour
{
    [SerializeField] float liveTime = 5f;
    Rigidbody body = null;

    //const float syncSpeedMultipler = 10.45f;   //Этот коэфициент отвечает за синхронизацию скоростей движения земли и самого обьекта
    //const float syncSpeedMultipler = 10.1f;   //Этот коэфициент отвечает за синхронизацию скоростей движения земли и самого обьекта

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        Destroy(gameObject, liveTime);
    }
    void Update()
    {
        //я рассматриваю скорости точки игрока, т.к. игрок статичен, => перемещаться будет блок.
        //Тем самым создается имитация того, что это игрок перемещается
        body.velocity = Vector3.back * GameManager.instance.ArcherSpeed * GameManager.enemyObstacleSpeedMultipler;
    }
}
