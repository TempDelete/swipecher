﻿using UnityEngine;

public class DeathZone : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Enemy")
            Destroy(other.gameObject);
    }
}
