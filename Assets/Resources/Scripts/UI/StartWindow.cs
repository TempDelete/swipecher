﻿using UnityEngine;
using UnityEngine.UI;

public class StartWindow : MonoBehaviour
{
    static bool isFirstGame = true;
    [SerializeField] Button startButton = null;
    void Start()
    {
        //при смерти игрока включается resultWindow 
        //А при загрузке сцены активно окно StartWindow
        //Эта проверка позволяет не нажимать два раза кнопку "Начать", грубо говоря. Чтобы понять, что поправляет
        //данный костыль, то достаточно закоментировать строчку в условии
        //и сыграть как минимум две сессии
        if (!isFirstGame)
        {
            startButton.onClick?.Invoke();
        }
        isFirstGame = false;
    }
}
