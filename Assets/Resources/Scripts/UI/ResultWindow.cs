﻿using UnityEngine;
using UnityEngine.UI;

public class ResultWindow : MonoBehaviour
{
    [SerializeField] Text resultText = null;

    void OnEnable()
    {
        resultText.text = GameManager.instance.enemyKilled.ToString();
    }
}
