﻿using System;
using UnityEngine;

public delegate void swipeEvent(Vector2 swipeVec);

public class SwipeManager : MonoBehaviour
{
    public float minSwipeLength = 200f;
    Vector2 currentSwipe;

    private Vector2 fingerStart;
    private Vector2 fingerEnd;

    public event swipeEvent onSwipe;
    public event Action onTap;

    public static SwipeManager instance = null;
    private void Awake()
    {
        onSwipe = null;
        onTap = null;
        if (instance != null)
        {
            Debug.LogError("Multiple SwipeManagers detected");
            Destroy(this);
        }
        else instance = this;
    }
    void Update()
    {
        SwipeDetection();
    }

    public void SwipeDetection()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fingerStart = Input.mousePosition;
            fingerEnd = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            fingerEnd = Input.mousePosition;

            currentSwipe = new Vector2(fingerEnd.x - fingerStart.x, fingerEnd.y - fingerStart.y);
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (currentSwipe.magnitude < minSwipeLength)
            {
                onTap?.Invoke();
            }
            else
               onSwipe?.Invoke(currentSwipe.normalized);
        }
    }
}
